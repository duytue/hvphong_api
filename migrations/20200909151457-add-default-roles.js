'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return [
      queryInterface.bulkInsert('roles', [
        { name: "admin", createdAt: new Date(), updatedAt: new Date() },
        { name: "user", createdAt: new Date(), updatedAt: new Date() },
        { name: "guest", createdAt: new Date(), updatedAt: new Date() }
      ])
    ];
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
