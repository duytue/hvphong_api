'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    queryInterface.changeColumn(
      'products',
      'spread',
      {
        type: Sequelize.STRING,
        allowNull: false,
      }
    ),
      queryInterface.changeColumn(
        'products',
        'price',
        {
          type: Sequelize.STRING,
          allowNull: false,
        }
      )
  },

  down: async (queryInterface, Sequelize) => {
    queryInterface.changeColumn(
      'products',
      'spread',
      {
        type: Sequelize.DOUBLE,
        allowNull: false,
      }
    ),
      queryInterface.changeColumn(
        'products',
        'price',
        {
          type: Sequelize.DOUBLE,
          allowNull: false,
        }
      )
  }
};
