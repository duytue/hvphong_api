'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    queryInterface.addColumn(
      'users',
      'isActive',
      Sequelize.BOOLEAN
    )
  },

  down: async (queryInterface, Sequelize) => {
    queryInterface.removeColumn('users', 'isActive')
  }
};
