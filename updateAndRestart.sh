#!/bin/bash

# any future command that fails will exit the script
set -e

# cd to repo
cd /home/ubuntu/HVPhong_Project/hvphong_api

# clone the repo again
echo "Pulling source code"
git pull origin master
echo "Source code pulled successfully"

#install npm packages
echo "Running npm install"
npm install
echo "npm install done"

export PORT=$PORT
export DB_USERNAME=$DB_USERNAME
export DB_PASSWORD=$DB_PASSWORD
export DB_HOST=$DB_HOST

#Restart the node server
echo "restart server"
pm2 restart api
echo "Restart server successfully"

pm2 list
echo "Deploy done"