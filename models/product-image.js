const Sequelize = require('sequelize');

const sequelize = require('../util/database');

const ProductImage = sequelize.define('product-image', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    url: {
        type: Sequelize.STRING(5000)
    }
});

module.exports = ProductImage;