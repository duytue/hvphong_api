const Sequelize = require('sequelize');

const sequelize = require('../util/database');

const ProductType = sequelize.define('product-type', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    description: {
        type: Sequelize.STRING(500)
    }
});

module.exports = ProductType;