const Sequelize = require('sequelize');

const sequelize = require('../util/database');

const Product = sequelize.define('product', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    title: Sequelize.STRING(500),
    spread: {
        type: Sequelize.STRING
    },
    area: {
        type: Sequelize.STRING(500),
        allowNull: false
    },
    price: {
        type: Sequelize.STRING,
        allowNull: false
    },
    description: {
        type: Sequelize.STRING(5000),
        allowNull: false
    },
    isDeleted: {
        type: Sequelize.BOOLEAN
    }
});

module.exports = Product;