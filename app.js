require('dotenv').config();
const path = require('path');
const cors = require('cors')
const express = require('express');
const Sentry = require('@sentry/node');
const Tracing = require("@sentry/tracing");
const bodyParser = require('body-parser');
const passport = require('passport');
const morgan = require('morgan');

const errorController = require('./controllers/error');
const sequelize = require('./util/database');
const adminRoutes = require('./routes/admin');
const shopRoutes = require('./routes/shop');
const userRoutes = require('./routes/user');
const Product = require('./models/product');
const ProductType = require('./models/product-type');
const ProductImage = require('./models/product-image');
const { strategy } = require('./middlewares/passportMiddleware');
const User = require('./models/user');
const Role = require('./models/role');
const winston = require('./config/winston');
const { SENTRY_URL } = require('./config/index');
const PORT = process.env.PORT || 3001;

const app = express();

Sentry.init({
    dsn: SENTRY_URL,
    integrations: [
        // enable HTTP calls tracing
        new Sentry.Integrations.Http({ tracing: true }),
        // enable Express.js middleware tracing
        new Tracing.Integrations.Express({ app }),
    ],
    tracesSampleRate: 1.0,
    environment: process.env.NODE_ENV || 'development',
    enabled: process.env.NODE_ENV === 'production' ? true : false
});

app.use(cors());

app.set('view engine', 'ejs');
app.set('views', 'views');

app.use(morgan('combined', { stream: winston.stream }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(passport.initialize());
passport.use(strategy);
app.use(express.static(path.join(__dirname, 'public')));

app.use(Sentry.Handlers.requestHandler());
app.use(Sentry.Handlers.tracingHandler());
app.use('/admin', adminRoutes);
app.use(shopRoutes);
app.use('/admin', userRoutes);
app.use(errorController);
app.use(Sentry.Handlers.errorHandler());

ProductType.hasMany(Product);
Product.hasMany(ProductImage, { as: 'productImages' });
User.belongsTo(Role);
User.hasMany(Product);

sequelize.sync({ force: false })
    .then(() => {
        app.listen(PORT, () => {
            console.log(`=== API is listening on port ${PORT} ===`);
        })
    })
    .catch(err => {
        console.log(err)
    })