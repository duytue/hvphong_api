const express = require('express');

const adminController = require('../controllers/admin');
const verifyToken = require('../middlewares/authMiddleware');

const router = express.Router();

router.post('/add-product', verifyToken, adminController.postAddProduct);

router.post('/edit-product', verifyToken, adminController.postEditProduct);

router.post('/add-product-type', verifyToken, adminController.postAddProductType);

router.post('/edit-product-type', verifyToken, adminController.postEditProductType);

router.post('/delete-product', verifyToken, adminController.deleteProduct);

router.post('/delete-product-type', verifyToken, adminController.deleteProductType);

router.get('/get-product-type/:productTypeId', adminController.getProductType);

router.get('/get-product-types', adminController.getProductTypes);

router.post('/add-product-image', verifyToken, adminController.postAddProductImage);

router.post('/update-product-image', verifyToken, adminController.postUpdateProductImage);

module.exports = router;