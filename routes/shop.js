const express = require('express');

const shopController = require('../controllers/shop');

const router = express.Router();

router.get('/products', shopController.getProducts);

router.get('/product/:productId', shopController.getProduct);

router.get('/get-product-by-type/:productTypeId', shopController.getProductByProductType);

module.exports = router;