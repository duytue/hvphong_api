module.exports = {
    SECRET_KEY: process.env.SECRET_KEY || '3e47c324049f3cf1ff9594c84f8e74de',
    DB_USERNAME: process.env.DB_USERNAME || 'root',
    DB_PASSWORD: process.env.DB_PASSWORD || 'password',
    DB_HOST: process.env.DB_HOST || 'localhost',
    SENTRY_URL: 'https://ea07501605894081af73574187a929ce@o446295.ingest.sentry.io/5424514'
}