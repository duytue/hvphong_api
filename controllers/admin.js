const Product = require('../models/product');
const ProductType = require('../models/product-type');
const ProductImage = require('../models/product-image');
const { ADMIN } = require('../constants/roles');
const { INSUFFICIENT_PERMISSION_MESSAGE, PROCESS_ERROR_MESSAGE } = require('../constants/messages');
const errorHelper = require('../helpers/errorHelper');

exports.postAddProduct = (req, res, next) => {
    const title = req.body.title;
    const spread = req.body.spread;
    const area = req.body.area;
    const price = req.body.price;
    const description = req.body.description;
    const productTypeId = req.body.productTypeId;

    Product.create({
        title: title,
        spread: spread,
        area: area,
        price: price,
        description: description,
        productTypeId: productTypeId,
        userId: req.userId
    })
        .then(result => {
            res.send({
                success: true,
                data: result
            })
        })
        .catch(err => {
            res.send({
                success: false,
                data: err
            })
        })
};

exports.postEditProduct = (req, res, next) => {
    const id = req.body.id
    const updatedTitle = req.body.title;
    const updatedSpread = req.body.spread;
    const updatedArea = req.body.area;
    const updatedPrice = req.body.price;
    const updatedDescription = req.body.description;
    const updatedType = req.body.type;
    const userId = req.userId;
    const roleId = req.roleId;

    Product.findById(id)
        .then(product => {
            if (roleId == ADMIN || product.userId === userId) {
                product.title = updatedTitle;
                product.spread = updatedSpread;
                product.area = updatedArea;
                product.price = updatedPrice;
                product.description = updatedDescription;
                product.type = updatedType;

                return product.save();
            }
            return false;
        })
        .then(result => {
            if (!result) {
                return errorHelper.permissionInsufficient(req, res, next, INSUFFICIENT_PERMISSION_MESSAGE);
            }
            res.send({
                success: true,
                data: result
            })
        })
        .catch(err => {
            res.send({
                success: false,
                data: err
            })
        })
}

// NOTE: hard delete product - use with caution!, currently, it's not published to api endpoint
exports.hardDeleteProduct = (req, res, next) => {
    const { id } = req.body;

    Product.findById(id)
        .then(product => {
            return product.destroy();
        })
        .then(result => {
            res.send({
                success: true,
                message: "Product deleted!"
            });
        })
        .catch(err => {
            console.log(err)
        })
};

// soft delete product
exports.deleteProduct = (req, res, next) => {
    const { id } = req.body;
    const { userId, roleId } = req;

    if (!id) {
        return res.status(400).send({
            success: false,
            message: "Product Id is required."
        })
    }

    Product.findById(id, {
        include: [
            { model: ProductImage, as: 'productImages' },
        ],
    })
        .then(product => {
            if (roleId === ADMIN || product.userId === userId) {
                product.isDeleted = !product.isDeleted;

                return product.save();
            }

            return false;
        })
        .then(result => {
            if (!result) {
                return errorHelper.permissionInsufficient(req, res, next, INSUFFICIENT_PERMISSION_MESSAGE);
            }
            res.send({
                success: true,
                message: "Product deleted!",
                data: result
            });
        })
        .catch(err => {
            res.status(500).send({
                success: false,
                message: PROCESS_ERROR_MESSAGE
            })
        })
};

exports.postAddProductType = (req, res, next) => {
    const { description } = req.body;
    const { roleId } = req;

    if (roleId !== ADMIN) {
        return errorHelper.permissionInsufficient(req, res, next, INSUFFICIENT_PERMISSION_MESSAGE);
    }

    ProductType.create({ description: description })
        .then(result => {
            res.send({
                success: true,
                data: result
            })
        })
        .catch(err => {
            res.send({
                success: false,
                data: err
            })
        })

};

exports.postEditProductType = (req, res, next) => {
    const id = req.body.id
    const updatedDescription = req.body.description;
    const { roleId } = req;

    if (roleId !== ADMIN) {
        return errorHelper.permissionInsufficient(req, res, next, INSUFFICIENT_PERMISSION_MESSAGE);
    }

    if (!id) {
        return res.status(400).send({
            success: false,
            message: "Product Type Id is required."
        })
    }

    ProductType.findById(id)
        .then(productType => {
            productType.description = updatedDescription;
            return productType.save();
        })
        .then(result => {
            res.send({
                success: true,
                data: result
            })
        })
        .catch(err => {
            return errorHelper.requestProcessError(req, res, next, PROCESS_ERROR_MESSAGE)
        });
};

exports.deleteProductType = (req, res, next) => {
    const { id } = req.body;
    const { roleId } = req;

    if (roleId !== ADMIN) {
        return errorHelper.permissionInsufficient(req, res, next, INSUFFICIENT_PERMISSION_MESSAGE)
    }

    ProductType.findById(id)
        .then(productType => {
            return productType.destroy();
        })
        .then(result => {
            res.send({
                success: true,
                message: "Product Type deleted!",
                data: result
            });
        })
        .catch(err => {
            return errorHelper.requestProcessError(req, res, next, PROCESS_ERROR_MESSAGE)
        })
};

exports.getProductTypes = (req, res, next) => {
    ProductType.findAll()
        .then(productTypes => {
            res.send({
                success: true,
                data: productTypes
            })
        })
        .catch(err => {
            res.send({
                success: false,
                data: err
            })
        });
};

exports.getProductType = (req, res, next) => {
    const id = req.params.productTypeId;

    ProductType.findById(id)
        .then(productType => {
            res.send({
                success: true,
                data: productType
            })
        })
        .catch(err => {
            res.send({
                success: true,
                data: err
            })
        });
};

exports.postAddProductImage = (req, res, next) => {
    const url = req.body.url;
    const productId = req.body.productId;

    ProductImage.create({
        url: url,
        productId: productId
    })
        .then(result => {
            res.send({
                success: true,
                data: result
            })
        })
        .catch(err => {
            res.send({
                success: false,
                data: err
            })
        })
};

exports.postUpdateProductImage = (req, res, next) => {
    const { productId, imageUrls } = req.body;

    ProductImage.findOne({ where: { productId: productId } })
        .then(productImage => {
            productImage.url = imageUrls;

            return productImage.save();
        })
        .then(result => {
            res.send({
                success: true,
                data: result
            })
        })
        .catch(err => {
            res.send({
                success: false,
                data: err
            })
        })
};