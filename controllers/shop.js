const { Sequelize } = require('sequelize');
const jwt = require('jsonwebtoken');
const { ADMIN } = require('../constants/roles');
const Op = Sequelize.Op

const Product = require('../models/product');
const ProductImage = require('../models/product-image');
const { SECRET_KEY } = require('../config');

exports.getProducts = async (req, res, next) => {
    const { access_token } = req.headers;
    let query = {};
    if (!access_token) {
        query = {
            include: [
                { model: ProductImage, as: 'productImages' },
            ],
            where: { isDeleted: { [Op.not]: true } }
        }
    } else {
        jwt.verify(access_token, SECRET_KEY, async (err, decoded) => {
            const { id, role } = decoded;

            if (role === ADMIN) {
                query = {
                    include: [
                        { model: ProductImage, as: 'productImages' },
                    ],
                }
            } else {
                query = {
                    include: [
                        { model: ProductImage, as: 'productImages' },
                    ],
                    where: { userId: id }
                }
            }
        });
    }

    try {
        const products = await Product.findAll(query);
        res.send({
            success: true,
            data: products
        });
    } catch (error) {
        res.send({
            success: true,
            data: error
        })
    }
};

exports.getProduct = (req, res, next) => {
    const id = req.params.productId;

    Product.findById(id, { include: [{ model: ProductImage, as: 'productImages' }] })
        .then(product => {
            res.send({
                success: true,
                data: product
            })
        })
        .catch(err => {
            res.send({
                success: true,
                data: err
            })
        });
};

exports.getProductByProductType = (req, res, next) => {
    const id = req.params.productTypeId;

    Product.findAll({
        include: [{ model: ProductImage, as: 'productImages' }],
        where: {
            productTypeId: id,
            isDeleted: { [Op.not]: true }
        }
    })
        .then(products => {
            res.send({
                success: true,
                data: products
            })
        })
        .catch(err => {
            res.send({
                success: true,
                data: err
            })
        });
};